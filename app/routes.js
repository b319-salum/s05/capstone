const { exchangeRates } = require('../src/util.js');

module.exports = (app) => {
	app.get('/', (req, res) => {
		return res.send({'data': {} });
	});

	app.get('/rates', (req, res) => {
		return res.send({
			rates: exchangeRates
		});
	})

	app.post('/currency', (req, res) => {



		// CURRENCY NAME
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter Name'
			})
		}

		if(typeof req.body.name !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : NAME has to be string' 
			})
		}

		if(req.body.name.length === 0){
			return res.status(400).send({
				'error' : 'Bad Request : Name must be filled' 
			})
		}

		// CURRENCY EXCHANGE
		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter ex'
			})
		}

		if(typeof req.body.ex !== 'object'){
			return res.status(400).send({
				'error' : 'Bad Request : ex has to be object' 
			})
		}

		if(req.body.ex === {}){
			return res.status(400).send({
				'error' : 'Bad Request : ex must have content' 
			})
		}

		// ALIAS
		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error' : 'Bad Request: missing required parameter alias'
			})
		}

		if(typeof req.body.alias !== 'string'){
			return res.status(400).send({
				'error' : 'Bad Request : ALIAS has to be string' 
			})
		}

		if(req.body.alias.length === 0){
			return res.status(400).send({
				'error' : 'Bad Request : ALIAS must be filled' 
			})
		}

		// DUPLICATE
		if(findAlias.includes(req.body.alias)){
			return res.status(400).send({
				'error' : 'Bad Request : Duplicate alias'
			})
		}

		if(!findAlias){
			return res.status(200).send({
				data: req.body
			})
		}

	})
}
