const chai = require('chai');
const expect = chai.expect;
const http = require('chai-http');
chai.use(http);

describe('forex_api_test_suite', () => {
	
	it('test_api_get_rates_is_running', () => {
		chai.request('http://localhost:5001').get('/rates')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})
	
	it('test_api_get_rates_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			done();	
		})		
	})
	
	it('test_api_get_rates_returns_object_of_size_5', (done) => {
		chai.request('http://localhost:5001')
		.get('/rates')
		.end((err, res) => {
			expect(Object.keys(res.body.rates)).does.have.length(5);
			done();	
		})		
	})
	
	it('test_api_post_currency_is_200', () => {
		chai.request('http://localhost:5001')
		.post('/currency')
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		})
	})

	it('test_app_post_currency_returns_400_if_no_currency_name', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'ex': {
		        'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_app_post_currency_returns_400_if_currency_name_is_not_a_string', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'name': 25,
			'ex': {
		        'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_app_post_currency_returns_400_if_currency_name_is_an_empty_string', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'name': "",
			'ex': {
		        'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_app_post_currency_returns_400_if_no_currency_ex', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'name': 'Japanese Yen'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_app_post_currency_returns_400_if_currency_ex_not_an_object', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'name': 'Japanese Yen',
			'ex': 'peso = 0.47'
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_app_post_currency_returns_400_if_no_ex_content', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'name': 'Japanese Yen',
			'ex': {}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_app_post_currency_returns_400_if_no_currency_alias', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'name': 'Japanese Yen',
			'ex': {
		        'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

	it('test_app_post_currency_returns_400_if_currency_alias_not_a_string', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 2999,
			'name': 'Japanese Yen',
			'ex': {
		        'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})

		it('test_app_post_currency_returns_400_if_currency_alias_is_an_empty_string', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': "",
			'name': 'Japanese Yen',
			'ex': {
		        'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})


	it('test_app_post_currency_returns_400_if_duplicate_alias_found', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'yuan',
			'name': 'Chinese Yuan',
      		'ex': {
		        'peso': 7.21,
		        'usd': 0.14,
		        'won': 168.85,
		        'yen': 15.45
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(400)
			done()
		})
	})


	it('test_app_post_currency_returns_200_if_complete_input_given', (done) =>{
		chai.request('http://localhost:5001')
		.post('/currency')
		.type('json')
		.send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
		        'peso': 0.47,
		        'usd': 0.0092,
		        'won': 10.93,
		        'yuan': 0.065
      		}
		})
		.end((err, res) => {
			expect(res.status).to.equal(200)
			done()
		})
	})

	it("test_api_currency_post_returns_submitted_object_to_show_submission_was_written", (done) => {
        chai.request('http://localhost:5001')
        .post('/currency')
        .type('json')
        .send({
			'alias': 'riyadh',
			'name': 'Saudi Arabian Riyadh',
			'ex': {
				'peso': 0.47,
				'usd': 0.00092,
				'won': 10.93,
				'yuan': 0.0065
			}
        })
        .end((err,res) => {
            expect(res.body).to.deep.equal({
				data:{
					'alias': 'riyadh',
					'name': 'Saudi Arabian Riyadh',
					'ex': {
						'peso': 0.47,
						'usd': 0.00092,
						'won': 10.93,
						'yuan': 0.0065
					}
				}
			});
            done();
        })
    })

})
